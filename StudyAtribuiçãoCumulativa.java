import java.util.Locale;
import java.util.Scanner;

/*
* Valores de atribuição cumulativa
 * a += b; o mesmo que a = a + b;
 * a -= b; o mesmo que a = a - b;
 * a *= b; o mesmo que a = a  b;
 * a /= b; o mesmo que a = a / b;
 * a %= b; o mesmo que a = a % b;
*/
public class StudyAtribuiçãoCumulativa {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);

        Scanner scanner = new Scanner(System.in);

        int minutos = scanner.nextInt();
        double conta = 50.00;

        if (minutos > 100) {
            conta += conta + (minutos - 100) * 2;
        }
        System.out.printf("Valor da conta = R$ %.2f%", conta);

    }
}
