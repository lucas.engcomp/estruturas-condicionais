import java.util.Scanner;

public class StudyWhile2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int soma = 0;
        int x = scanner.nextInt();

        while (x != 0) {
            soma += x;
            x = scanner.nextInt();
        }
        System.out.println(soma);
        scanner.close();
    }
}
