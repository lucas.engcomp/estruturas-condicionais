import java.util.Scanner;

/*
* Escreva um programa em Java que solicita 10 números ao usuário, através de um laço while, e ao final
mostre qual destes números é o maior.
**/
public class StudyWhile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numero, contador = 0, maior = 0;

        while (contador < 10) {
            System.out.println("Informe um número para comparação");
            numero = scanner.nextInt();
            //contador = contador + 1;
            contador++;
            if (numero > maior) {
                maior = numero;
            }
            System.out.println("Até agora o maior número é: " + maior);
        }
    }
}
