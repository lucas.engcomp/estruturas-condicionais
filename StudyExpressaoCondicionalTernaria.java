import java.util.Scanner;

/* Estrutura
* ( condição ) ? valor_se_verdadeiro : valor_se_falso
* */
public class StudyExpressaoCondicionalTernaria {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double preco = 35.5;
        double desconto;

        /*
        if (preco < 20.00) {
            desconto = preco * 0.1;
        } else {
            desconto = preco * 0.05;
        }
        */

        desconto = (preco < 20) ? preco * 0.1 : preco * 0.05;

        System.out.println(desconto);
    }
}
